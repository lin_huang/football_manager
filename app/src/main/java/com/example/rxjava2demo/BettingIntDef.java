package com.example.rxjava2demo;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class BettingIntDef {
    public static final int FOOTBALL = 1;
    public static final int BASKETBALL = 2;


    @IntDef({FOOTBALL, BASKETBALL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {

    }

    public static int getType(@Type int type){
        switch (type) {
            case FOOTBALL : return FOOTBALL;
            case BASKETBALL : return BASKETBALL;
            default: return -1;
        }
    }
}
