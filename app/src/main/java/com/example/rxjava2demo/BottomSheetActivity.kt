package com.example.rxjava2demo

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.activity_bottom_sheet.*

class BottomSheetActivity : AppCompatActivity(), FunctionAdapter.ItemClickListener,
    FavoriteTeamViewHolder.ItemClickListener {

    val football: MutableList<FavoriteTeamData> = mutableListOf(
        FavoriteTeamData(1),
        FavoriteTeamData(2),
        FavoriteTeamData(3)
    )

    val basketball: MutableList<FavoriteTeamData> = mutableListOf(
        FavoriteTeamData(1),
        FavoriteTeamData(2),
        FavoriteTeamData(3)
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_sheet)

        fabFootBallManager.setOnClickListener {
            addTeam.addFavoriteTeam(
                BettingType.FOOTBALL,
                FavoriteTeamData((5..10).random())
            )
        }

        basketBall.setOnClickListener {
            addTeam.addFavoriteTeam(
                BettingType.BASKETBALL,
                FavoriteTeamData((5..10).random())
            )
        }

        /**
         copy below this
          */
        initSportTypeUnScrollViewPager()

        initSportTypeSelectorUI(BettingType.FOOTBALL)

        initBottomViewFavoriteTeam()
    }

    private fun initSportTypeUnScrollViewPager() {
        vp_unScroll.adapter = UnScrollViewPagerAdapter(supportFragmentManager)
        vp_unScroll.setPagingEnabled(false)
    }

    private fun initSportTypeSelectorUI(type: BettingType) {
        tv_favorite_team_football.text = getString(R.string.football)
        tv_favorite_team_basketball.text = getString(R.string.basketball)
        v_favorite_team_football_under_line.setBackgroundColor(ContextCompat.getColor(this, R.color.color_00a826))
        v_favorite_team_basketball_under_line.setBackgroundColor(ContextCompat.getColor(this, R.color.color_00a826))
        sportTypeUIOnClick(type)

        tv_favorite_team_football.setOnClickListener {
            sportTypeUIOnClick(type)

        }
        tv_favorite_team_basketball.setOnClickListener {
            sportTypeUIOnClick(BettingType.BASKETBALL)
        }
    }

    private fun sportTypeUIOnClick(type: BettingType) {
        when (type) {
            BettingType.FOOTBALL -> {
                tv_favorite_team_football.setTextColor(ContextCompat.getColor(this, R.color.color_00a826))
                v_favorite_team_football_under_line.visibility = View.VISIBLE
                tv_favorite_team_basketball.setTextColor(ContextCompat.getColor(this, R.color.color_999999))
                v_favorite_team_basketball_under_line.visibility = View.GONE
            }
            BettingType.BASKETBALL -> {
                tv_favorite_team_football.setTextColor(ContextCompat.getColor(this, R.color.color_999999))
                v_favorite_team_football_under_line.visibility = View.GONE
                tv_favorite_team_basketball.setTextColor(ContextCompat.getColor(this, R.color.color_00a826))
                v_favorite_team_basketball_under_line.visibility = View.VISIBLE
            }
        }
        vp_unScroll.setCurrentItem(type.ordinal, true)
    }

    private fun showFootBallManagerDashBoard() {
        FootBallDashBoardBSDialog(
            this
            , listOf(
                FootBallDashBoardBSDialog.FootBallFunctionData(
                    FootBallDashBoardBSDialog.Function.BETTING,
                    true
                )
                ,
                FootBallDashBoardBSDialog.FootBallFunctionData(
                    FootBallDashBoardBSDialog.Function.INFO,
                    true
                )
                ,
                FootBallDashBoardBSDialog.FootBallFunctionData(
                    FootBallDashBoardBSDialog.Function.CALENDAR,
                    false
                )
                ,
                FootBallDashBoardBSDialog.FootBallFunctionData(
                    FootBallDashBoardBSDialog.Function.ACCOUNT,
                    false
                )
                ,
                FootBallDashBoardBSDialog.FootBallFunctionData(
                    FootBallDashBoardBSDialog.Function.HISTORY,
                    false
                )

                ,
                FootBallDashBoardBSDialog.FootBallFunctionData(
                    FootBallDashBoardBSDialog.Function.SETTING,
                    false
                )
            )
            , this
        ).show()
    }

    private fun initBottomViewFavoriteTeam() {
        addTeam.initData(football, basketball, this)
    }

    override fun footBallManagerFunctionOnClick(function: FootBallDashBoardBSDialog.Function?) {
        Toast.makeText(this, function?.id.toString(), Toast.LENGTH_SHORT).show()
    }

    override fun favoriteTeamOnRemove(
        data: FavoriteTeamViewHolder.IFavoriteTeamViewHolder,
        type: BettingType
    ) {
        Toast.makeText(this, type.name + " / " + data.getTeamId().toString(), Toast.LENGTH_SHORT)
            .show()
        addTeam.removeFavoriteTeam(type, data.getTeamId())
    }
}