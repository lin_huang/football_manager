package com.example.rxjava2demo

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_sport_type.*

/**
 * A simple [Fragment] subclass.
 * Use the [SportTypeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SportTypeFragment : Fragment() {

    companion object {
        private const val BUNDLE_SPORT_TYPE = "BUNDLE_SPORT_TYPE"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param type SportType enum
         * @return A new instance of fragment SportTypeFragment.
         */
        @JvmStatic
        fun newInstance(type: BettingType) =
            SportTypeFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(BUNDLE_SPORT_TYPE, type)
                }
            }
    }

    private lateinit var sportType: BettingType

    private val footBallLegend = arrayOf("英超","西甲","德甲","意甲","法甲","欧冠杯")
    private val basketBallLegend = arrayOf("NBA","CBA","NCAA","SBL")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            sportType = it.getSerializable(BUNDLE_SPORT_TYPE) as BettingType
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sport_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val legends = when(sportType) {
            BettingType.FOOTBALL -> footBallLegend
            BettingType.BASKETBALL -> basketBallLegend
        }

        // tab layout
        tl_favorite_team_legend.setBackgroundColor(requireContext().getColor(R.color.color_444444))
        tl_favorite_team_legend.setSelectedTabIndicatorColor(requireContext().getColor(android.R.color.transparent))
        tl_favorite_team_legend.setTabTextColors(requireContext().getColor(R.color.color_999999), requireContext().getColor(R.color.color_00a826))

        vp_favorite_team_content.adapter = SportTypePagerAdapter(requireFragmentManager(), lifecycle, legends, sportType)
        TabLayoutMediator(tl_favorite_team_legend, vp_favorite_team_content) { tab, position ->
            tab.text = legends[position]
        }.attach()
    }
}

class SportTypePagerAdapter(
    fg: FragmentManager,
    lifecycle: Lifecycle,
    private val legends: Array<String>,
    private val type: BettingType
) : FragmentStateAdapter(fg, lifecycle) {
    override fun getItemCount(): Int {
        return legends.size
    }

    override fun createFragment(position: Int): Fragment {
        return SportTeamFragment.newInstance(type)
    }
}