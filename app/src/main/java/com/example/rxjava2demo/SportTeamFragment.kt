package com.example.rxjava2demo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_favorite_team_legend.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SportTeamFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SportTeamFragment : Fragment(), SelectableFavoriteTeamAdapter.ItemClickListener {

    private var sportType: BettingType? = null

    val globalData = mutableListOf(
        FavoriteTeamData(1, true),
        FavoriteTeamData(2),
        FavoriteTeamData(3, true)
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            sportType = it.getSerializable(BUNDLE_SPORT_TYPE) as BettingType
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite_team_legend, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e("test", "fragment data: $globalData")

        rv_favorite_team.adapter = SelectableFavoriteTeamAdapter(globalData, this, BettingType.FOOTBALL)
        rv_favorite_team.addItemDecoration(SelectableFavoriteTeamItemDecoration())
    }

    companion object {

        private const val BUNDLE_SPORT_TYPE = "BUNDLE_SPORT_TYPE"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param type SportType enum
         * @return A new instance of fragment SportTypeFragment.
         */
        @JvmStatic
        fun newInstance(type: BettingType) =
            SportTeamFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(BUNDLE_SPORT_TYPE, type)
                }
            }
    }

    override fun onClick(data: SelectableFavoriteTeamViewHolder.IViewHolder, type: BettingType) {
        Log.e("test", "fragment  receive data: $data")
        Log.e("test", "id: ${data.getTeamId()} / type: ${type.name} / isSelected: ${data.isSelected()}")

        globalData.forEach {
            Log.e("test", "globalData data receive: $it / ${it.isSelected()}")
        }
    }
}