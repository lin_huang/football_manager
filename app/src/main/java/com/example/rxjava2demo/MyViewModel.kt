package com.example.rxjava2demo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.NullPointerException

class MyViewModel {
    private val user: MutableLiveData<String> = MutableLiveData()

    public fun getUserLiveData(): LiveData<String> {
        return user
    }

    public fun getUserApi(pk: String) {
        GlobalScope.launch {
            delay(3000)
            user.postValue("getUserApi finish")

        }
    }
}