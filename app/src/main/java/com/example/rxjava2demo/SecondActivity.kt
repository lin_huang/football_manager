package com.example.rxjava2demo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_second.*
import java.text.DecimalFormat
import java.util.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        etInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                inputLogicFilter(s)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })


        val stringData = listOf<String>("1", "2")
        val data = listOf<Test>(Test("1", "test1"), Test("2", "test2"), Test("3", "test3"))

        tvShow.text = stringData.joinToString(separator = ",") {
            it
        }

//        tvShow.text = data.flatMap {
//            stringData
//        }.toString()
//
//        val s = "789"
//        tvShow.text = s.substring(s.indexOf("-").inc()).toMask()

        if (0.123456 > 0.0) {
            val exchangeRate = 0.123.toString()
            if (!exchangeRate.isNotEmpty() || !exchangeRate.contains(".", true)) return

            val addZeroTimes = exchangeRate.length.dec() - exchangeRate.indexOf(".")
            var pattern = "#,##0."
            if (addZeroTimes > 0) {
                for (i in 0 until if(addZeroTimes > 4) 4 else addZeroTimes ) {
                    pattern += "0"
                }
            } else  pattern += "0"
            tvShow.text = DecimalFormat(pattern).format(0.123456)
        }




//        val loadingDialog = AlertDialog.Builder(this).setTitle("1231").create()
//        loadingDialog.let {
//            it.show()
//            Timer().schedule(object : TimerTask() {
//                override fun run() {
//                    it.dismiss()
//                    this@SecondActivity.finish()
//                }
//            }, 3000)
//        }
    }

    private fun inputLogicFilter(s: Editable?) {
        // prevent startWith dot and 01 ~ 09
        if (s?.startsWith(".") == true
            || (s?.startsWith("0") == true &&
                    s.length == 2 &&
                    s.indexOf(".") != 1)
        ) {
            s.delete(0, 1)
        }

        // 最多只能小數點2位
        if (!s.isNullOrEmpty() && s.contains(".") && s.length - s.indexOf(".") > 3) {
            s.delete(s.length.dec(), s.length)
        }
    }
}

data class Test(val id: String, val name: String) {

}

fun String.toMask() :String{
    if (length > 3) {
        return substring(0,length-3).replace(Regex("[a-zA-Z0-9]"),"*") + substring(length-3)
    }
    return this
}
