package com.example.rxjava2demo

import android.graphics.Rect
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_favorite_team_large.view.*

class SelectableFavoriteTeamAdapter<T : SelectableFavoriteTeamViewHolder.IViewHolder>(
    private var favoriteTeam: MutableList<T>,
    private val listener: ItemClickListener,
    private val type: BettingType
) : RecyclerView.Adapter<SelectableFavoriteTeamViewHolder>() {

    interface ItemClickListener {
        fun onClick(data: SelectableFavoriteTeamViewHolder.IViewHolder, type: BettingType)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SelectableFavoriteTeamViewHolder {
        return SelectableFavoriteTeamViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_favorite_team_large, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return favoriteTeam.size
    }

    override fun onBindViewHolder(holder: SelectableFavoriteTeamViewHolder, position: Int) {
        holder.bind(favoriteTeam[position])
        holder.itemView.setOnClickListener {
            Log.e("test", "inner data: $favoriteTeam")
            Log.e("test", "inner data: ${favoriteTeam[position].isSelected()}")
            favoriteTeam[position].select()
            Log.e("test", "inner data: ${favoriteTeam[position].isSelected()}")
            listener.onClick(favoriteTeam[position], type)
            notifyItemChanged(position)
        }
    }
}

class SelectableFavoriteTeamViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    interface IViewHolder {
        fun teamIcon(): String
        fun isSelected(): Boolean
        fun select();
        fun teamName(): String
        fun getTeamId(): Int
    }

    fun bind(
        iViewHolder: IViewHolder
    ) {
        itemView.v_add_favorite_team_bg
            .setBackgroundResource(
                if (iViewHolder.isSelected())
                    R.drawable.ic_team_selected
                else
                    R.drawable.ic_color_222222
            )

        Glide.with(itemView.context)
            .load(iViewHolder.teamIcon())
            .into(itemView.iv_add_favorite_team_icon_large)

        itemView.tv_add_favorite_team_name.text = iViewHolder.teamName()
        itemView.tv_add_favorite_team_name.setTextColor(
            ContextCompat.getColor(
                itemView.context,
                if (iViewHolder.isSelected()) R.color.color_ffffff else R.color.color_888888
            )
        )
    }
}

class SelectableFavoriteTeamItemDecoration : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        if (parent.layoutManager !is GridLayoutManager) return

        val density = view.context.resources.displayMetrics.density
        val spanCount = (parent.layoutManager as GridLayoutManager).spanCount
        val childPosition = parent.getChildAdapterPosition(view)

        if  (childPosition < spanCount) outRect.top = density.times(14).toInt()

//        when (childPosition % spanCount) {
//            0 -> outRect.right = density.times(22).toInt()
//            spanCount.dec() -> outRect.left = density.times(22).toInt()
//            else -> {
//                outRect.left = density.times(22).toInt()
//                outRect.right = density.times(22).toInt()
//            }
//        }
    }
}