package com.example.rxjava2demo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.football_manager_bottom_sheet.*
import kotlinx.android.synthetic.main.item_function.view.*

class FootBallDashBoardBSDialog<T: CircleFunctionViewHolder.IFunctionViewHolder>(
    context: Context,
    functionList : List<T>,
    clickListener: FunctionAdapter.ItemClickListener
) : BottomSheetDialog(context) {

    companion object {
        private const val SPAN_COUNT = 3
    }

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.football_manager_bottom_sheet, null)
        setContentView(view)
        (view.parent as ViewGroup).setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent))
        rvContent.adapter = FunctionAdapter(functionList, clickListener)
        rvContent.layoutManager = GridLayoutManager(context, SPAN_COUNT , RecyclerView.VERTICAL, false)

        run out@ {
            functionList.forEach {
                if (it.isLock()) {
                    tvTitle.text = context.getString(R.string.function_lock_title)
                    return@out
                }
                tvTitle.text = context.getString(R.string.function_unlock_title)
            }
        }

    }

    class FootBallFunctionData(private val function: Function, private val lock: Boolean) :
        CircleFunctionViewHolder.IFunctionViewHolder {
        override fun lockIcon(): Int {
            return function.lockResId
        }

        override fun unlockIcon(): Int {
            return function.unlockResId
        }

        override fun functionName(): Int {
            return function.nameResId
        }

        override fun isLock(): Boolean {
            return lock
        }

        override fun getId(): Int {
            return function.id
        }
    }

    enum class Function(val id: Int, val lockResId: Int, val unlockResId: Int, val nameResId: Int) {
        BETTING(2, R.drawable.ic_lock, R.drawable.ic_unlock, R.string.betting_recommend),
        INFO(3, R.drawable.ic_lock, R.drawable.ic_unlock, R.string.game_information),
        CALENDAR(4, R.drawable.ic_lock, R.drawable.ic_unlock, R.string.game_calendar),
        ACCOUNT(5, R.drawable.ic_lock, R.drawable.ic_unlock, R.string.account_information),
        HISTORY(6, R.drawable.ic_lock, R.drawable.ic_unlock, R.string.history_query),
        SETTING(7, R.drawable.ic_lock, R.drawable.ic_unlock, R.string.manager_setting);

        companion object {
            fun getFunction(id: Int?): Function? {
                return if (null == id) null
                else
                    when (id) {
                        BETTING.id -> BETTING
                        INFO.id -> INFO
                        CALENDAR.id -> CALENDAR
                        ACCOUNT.id -> ACCOUNT
                        HISTORY.id -> HISTORY
                        SETTING.id -> SETTING
                        else -> null
                    }
            }
        }
    }
}

class FunctionAdapter<T : CircleFunctionViewHolder.IFunctionViewHolder>(
    private val functionList: List<T>,
    private val clickListener: ItemClickListener
) : RecyclerView.Adapter<CircleFunctionViewHolder>() {

    interface ItemClickListener {
        fun footBallManagerFunctionOnClick(function: FootBallDashBoardBSDialog.Function?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CircleFunctionViewHolder {
        return CircleFunctionViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_function, parent, false)
        ).apply {
            itemView.setOnClickListener {
                if (this.function?.isLock() != false) return@setOnClickListener
                clickListener.footBallManagerFunctionOnClick(FootBallDashBoardBSDialog.Function.getFunction(this.function?.getId()))
            }
        }
    }

    override fun getItemCount(): Int {
        return functionList.size
    }

    override fun onBindViewHolder(holder: CircleFunctionViewHolder, position: Int) {
        holder.bind(functionList[position])
    }
}

class CircleFunctionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    interface IFunctionViewHolder {
        fun lockIcon(): Int
        fun unlockIcon(): Int
        fun functionName(): Int
        fun isLock(): Boolean
        fun getId(): Int
    }

    var function: IFunctionViewHolder? = null

    fun bind(iFunction: IFunctionViewHolder) {
        function = iFunction
        itemView.tvFunctionName.setText(iFunction.functionName())
        itemView.ivFunctionIcon.setImageResource(if (iFunction.isLock()) iFunction.lockIcon() else iFunction.unlockIcon())
    }
}