package com.example.rxjava2demo

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.GestureDetectorCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.football_manager_add_favorite_team.view.*
import kotlinx.android.synthetic.main.item_favorite_team_small.view.*


class FootBallAddFavoriteTeamView : ConstraintLayout {

    private lateinit var gesture: GestureDetectorCompat

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    init {
        View.inflate(context, R.layout.football_manager_add_favorite_team, this)
        tv_favorite_team_number.text = context.getString(R.string.add_favorite_team_title, 0)
        tv_favorite_team_save.isEnabled = false
        tv_favorite_team_save.setOnClickListener {

        }

//        click.setOnTouchListener { v, event ->
//            gesture.onTouchEvent(event)
//            return@setOnTouchListener false
//        }
//
//        gesture = GestureDetectorCompat(context, MyGestureListener())

        rv_favorite_team_football.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

            }
        })
    }

    /**
     *init data
     * @param footballFavoriteTeam for football recyclerView adapter use
     * @param basketBallFavoriteTeam for football recyclerView adapter use
     * @param listener for recyclerViewHolder item click notify
     */
    fun <T : FavoriteTeamViewHolder.IFavoriteTeamViewHolder> initData(
        footballFavoriteTeam: MutableList<T>,
        basketBallFavoriteTeam: MutableList<T>,
        listener: FavoriteTeamViewHolder.ItemClickListener
    ) {
        setupUIVisible(footballFavoriteTeam.size, basketBallFavoriteTeam.size)
        setupFootBallRecyclerView(footballFavoriteTeam, listener)
        setupBasketBallRecyclerView(basketBallFavoriteTeam, listener)
    }

    private fun <T : FavoriteTeamViewHolder.IFavoriteTeamViewHolder> setupBasketBallRecyclerView(
        basketBallFavoriteTeam: MutableList<T>,
        listener: FavoriteTeamViewHolder.ItemClickListener
    ) {
        rv_favorite_team_basketball.adapter = FavoriteTeamAdapter(
            basketBallFavoriteTeam,
            listener,
            BettingType.BASKETBALL
        )
        (rv_favorite_team_basketball.adapter as RecyclerView.Adapter).notifyDataSetChanged()
    }

    private fun <T : FavoriteTeamViewHolder.IFavoriteTeamViewHolder> setupFootBallRecyclerView(
        footballFavoriteTeam: MutableList<T>,
        listener: FavoriteTeamViewHolder.ItemClickListener
    ) {
        rv_favorite_team_football.adapter = FavoriteTeamAdapter(
            footballFavoriteTeam,
            listener,
            BettingType.FOOTBALL
        )
        (rv_favorite_team_football.adapter as RecyclerView.Adapter).notifyDataSetChanged()
    }

    private fun setupUIVisible(
        footballFavoriteTeamCount: Int,
        basketBallFavoriteTeamCount: Int
    ) {
        tv_favorite_team_number.text = context.getString(
            R.string.add_favorite_team_title,
            footballFavoriteTeamCount.plus(basketBallFavoriteTeamCount)
        )
        tv_favorite_team_hint_title.visibility =
            if (footballFavoriteTeamCount.plus(basketBallFavoriteTeamCount) <= 0)
                View.VISIBLE else View.GONE
        tv_favorite_team_hint_content.visibility =
            if (footballFavoriteTeamCount.plus(basketBallFavoriteTeamCount) <= 0)
                View.VISIBLE else View.GONE

        tv_favorite_team_football.visibility =
            if (footballFavoriteTeamCount > 0) View.VISIBLE else View.GONE
        rv_favorite_team_football.visibility =
            if (footballFavoriteTeamCount > 0) View.VISIBLE else View.GONE
        tv_favorite_team_basketball.visibility =
            if (basketBallFavoriteTeamCount > 0) View.VISIBLE else View.GONE
        rv_favorite_team_basketball.visibility =
            if (basketBallFavoriteTeamCount > 0) View.VISIBLE else View.GONE
    }

    /**
     * @param type Use betting type enum to recognize which recyclerview adapter should use
     * @param teamId Use id to find out which data need to remove from data list
     */
    fun removeFavoriteTeam(type: BettingType, teamId: Int) {
        when (type) {
            BettingType.FOOTBALL ->
                (rv_favorite_team_football.adapter as FavoriteTeamAdapter<*>).removeFavoriteTeam(
                    teamId
                )

            BettingType.BASKETBALL ->
                (rv_favorite_team_basketball.adapter as FavoriteTeamAdapter<*>).removeFavoriteTeam(
                    teamId
                )
        }

        setupUIVisible(
            (rv_favorite_team_football.adapter as FavoriteTeamAdapter<*>).itemCount,
            (rv_favorite_team_basketball.adapter as FavoriteTeamAdapter<*>).itemCount
        )
    }

    /**
     * @param type Use betting type enum to recognize which recyclerview adapter should use
     * @param data add into adapter data list
     */
    fun <T : FavoriteTeamViewHolder.IFavoriteTeamViewHolder> addFavoriteTeam(
        type: BettingType,
        data: T
    ) {
        if ((rv_favorite_team_football.adapter as FavoriteTeamAdapter<*>).itemCount
                .plus((rv_favorite_team_basketball.adapter as FavoriteTeamAdapter<*>).itemCount) >= 10
        )
            return

        when (type) {
            BettingType.FOOTBALL ->
                (rv_favorite_team_football.adapter as FavoriteTeamAdapter<T>).addFavoriteTeam(
                    data
                )
            BettingType.BASKETBALL ->
                (rv_favorite_team_basketball.adapter as FavoriteTeamAdapter<T>).addFavoriteTeam(
                    data
                )
        }

        setupUIVisible(
            (rv_favorite_team_football.adapter as FavoriteTeamAdapter<*>).itemCount,
            (rv_favorite_team_basketball.adapter as FavoriteTeamAdapter<*>).itemCount
        )
    }
}

enum class BettingType {
    FOOTBALL, BASKETBALL
}

class FavoriteTeamAdapter<T : FavoriteTeamViewHolder.IFavoriteTeamViewHolder>(
    private var favoriteTeam: MutableList<T>,
    private val listener: FavoriteTeamViewHolder.ItemClickListener,
    private val type: BettingType
) : RecyclerView.Adapter<FavoriteTeamViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteTeamViewHolder {
        return FavoriteTeamViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_favorite_team_small, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return favoriteTeam.size
    }

    override fun onBindViewHolder(holder: FavoriteTeamViewHolder, position: Int) {
        holder.bind(favoriteTeam[position], listener, type)
    }

    fun removeFavoriteTeam(id: Int): Boolean {
        val removeIndex = favoriteTeam.indexOfFirst {
            id == it.getTeamId()
        }
        if (removeIndex >= 0) {
            favoriteTeam.removeAt(removeIndex)
            notifyItemRemoved(removeIndex)
        }

        return favoriteTeam.size == 0
    }

    fun addFavoriteTeam(data: T) {
        favoriteTeam.add(data)
        notifyItemInserted(favoriteTeam.size.minus(1))
    }
}

class FavoriteTeamData(private val id: Int,var selected: Boolean = false) : FavoriteTeamViewHolder.IFavoriteTeamViewHolder,  SelectableFavoriteTeamViewHolder.IViewHolder{
    override fun teamIcon(): String {
        return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAN8AAADiCAMAAAD5w+JtAAABiVBMVEX////etAcAAAAAUp/gOj6iIkv/3xvlugfcsgfnuwfitwcAVKP/4Rv/5BngMT8AOG7YTjhwFzSmH0daPnt2NWvdQTzlji7eJkD70R/rnivwxh7jii7tuyLrzhlgUACaJ1ODMWVfSwDoPEBQUVXgMTZFBxXfHyXfhIX08vLIyMhQRgk1RomUeAWmHkOvLTG1kwbHoQaoiAW2trbDngZENwKfgQWEhIR1XwSenp7i4uLd3d3PqAaCaQBsWANvb28uJQEfGQEdFwFRDSd/f3+pqalLPQI5OTlcXFw5LgKQkJAmHwEsLCyhoaEAQHw9PT0aGhoAABMUEAEAHUQ+MgIlAA04ABF9HyN4aQ3LsRXdwRdQSQgAABYACSmNHkFGPAAASY4ALFgaABBSKBO1LDOynBNUEhiZJyp/VRgNFgDnST5xGyDJNDgiJCuPJSi6pBOKeQ9ZOT3eVFevaGkMAAiVDROuGh5HJy5JLw5CEBNqGzk+K1UlMF9RJEoAEjM+AB8AACYAHzxzJEw3GjlTG260AAAZEklEQVR4nOVd+WPcxnUWhoqBAUnLdFs7Tq1tQDkOuUWCxR4ESWC5pMgll0uJhyhfkijS1dEkvtqybZw0UpT+5Z0bg3MxWNC7VL+fpCUwmA/z5l0z83DjRoVYvbM0dhtrh80KenIVWD8AAIzbuVXURgVv6QqwDwj2xunc2i5tZKeyXlWF5SPAUX4IV0Ubu+sV9m18rN0BEu6slWqEyLfAxvQI6doGiGFFvZGlvUQj08FwXXRsy/Hb/N+rao0srfAbLc0VDLfLCcIobKwUlv6lnVCoAtM0oQ1KMAzZARdqptENRb34ZF7e2C924Q5q92i/AMWlHWnaWb6uIeiGpfr610Px9jQTNwIb7bDhvWYBOV3expcWGpYlMcObOf1bb26HGhOAfgtqDNDpiZ8Pd0b1bW01FIB+gzdiyo0AcLCS15W1HTFDivDblRoGe/vN5TXRx6WltfXl5urKnkwNod6CpiZgws5J+Lc7O9ldW1+VHtZzoo24g+hDDvb27zWX19eWwu6sLe+sRDT3xmh6K0AVXkPuGOmcLjNEb2l1OT6Oa8v7EYXZcxONwFZNuS8jp+uyYoOWa+qxjlGG8dcPju5sbO/vr+6vbG8cxsYf9J04O8rQ6LSBGkZM+TV6ldkK4t1Lwabnamn94q/fK9inwM9pxeh0R7cA2rbv03/l86PXtExTh77rnWZTswLXh7GRQ/ZB/gENrDtawjxH1yONaPFWdR11pX+S2UI7cAyIetwh/zvMo0cnq62zhqHZcG3P6p/2NjeHw0G73be8wO44DU3XE1KJdN5wGJMzRNEJst9SP2jF2kFqBbRbiamIuqIbDacTeF2r3z4dDIdbm73Tfi2wHd/krxnSgc7xne6RC2pQbhm9PiigY17RUQrZtcGPPyIzCGN/QG+p1fH6WxFmQyvotNBfYkMFW6fgX3rASmgs1hcTd0DuTaQBnc6p5Sx61PIN9GTTo0DYPfzt4uJLJHB+ogHykkyj0XIwWr6B3rmeeEsmbFjg62fvzT9HaisxhkVg0JeXL52+asOmrnWGhN3Nm4s3v8MM0ztnMqS3Ahtosr5cXHxvfn7++QkYpCvm/J608iS0Sf7oqjWKRsZBcv/iGWaHQRjWVF8/EoA+YXfz5nvz78zNz3/5FL2n+OwcCRgQDul+Gh1cBelEU0tz8aT+7htGjjFEUjroxPVgHjnDPgEPX9JXhPghzM/f/lcAfuc5ZnyS5rZEjcSdnPELtYuZnCL8L0Sh+UQ1Xn7+zaLMDuOTXy8jWbecAhKGQg7DRUPX/er3rBXKDzH84N1nf0AdeorUP1VrmX0Rf2EaJsPP3qYCykbQtGtI+2pUaYaAWFE4ttfH1754mSRH+CFltfototjBRjLTeCPhbtioodPnaLz+4WaC383FxW++e0EtCbJKSC0lOqP5jl3nc4qJZ2b2hrryBn8+FdhTqxtQEOOzSX89Rp732o1PU8gxfjdwbIDc58tup0XVJR8Bkyp6aDg2Nv/157fn5+feSeWHpX3xXe37Hx7Rp/5uULdqXY91p2bR4RowkWPqZS+LHvPO+vzyDlheb+5v7O0eU3x7cGcP+ZDIi+de3qep9Dg/hKXlFfLS+h5yC1oN3zd8H5mJju09xT9/9vmzjzA5TCeD382bH/4cadT5udtfut99/ocXv/rs4SUle3Ja+6Lz/VfPkbvFOryZax5u0NA2VKFwmD5RQ4zkR7DeXNn7Nua6DF98/vIbPDgfcTo5/PALmJub/8WHiwTvfjAvAVhsPPQ85clATSCfH062L6DCjwHFjsur4PmXX311+535jz+kE7cwP8TwFx/SH979R3YJvuo58Flvqe7czu0wdWE8pmJgL9dbVeSHsQxuz89hfMz7Oh6/uTnQZcMHicY7yu8wS7H6YgDzUxpl+JHOVsVPGj4n3/nkIKEnF2nYy5+Bk+cn7LXey9edYQ8wGkyVu/kB8aT5zX8v9RSjQKrtjjyAev58nTi/0zaXtGGOZx3BujyAup0b8E+Y39xXwIkMXwF6N26QvFaXqVAjNyU1YX7zX/BwABJPplgCe1320mB/d2r5IeMQ6LJnVnBB5hBfy5MwuRpmsvyQdmHBOKyNNu1yJxA2eRiRt1AyYX61HuSzCKPwahO5ms1cvfvtlPKbewfYTLuQVatRznKIfUnD5AroRPlh8ZS1y0jXRYDGSTxTkbPaP1l+X5ww8fSLGweKXUlAYT/b6ZksP6E9bRXtgnFPiiLMHBM/SX6hcYdtNfFkAjqE3LZkBhGT5IdCB0PWngr0WBTBI4/sCThRfl8w60Ajo9GRg4xtKU8B2//26wx8ks7v5r9n3fAfFfLjE4jmJdS2PDWlCYju/yQDGfTQCGbgWWX8kPXryNNPbSsJyVOwvJvZAZk8FLH42+r4fcnzZqb69GMujMkVzLPp44fUC1MPDfXpx4IkFgQa4GV6Hnei/H4AUAr9Cu7tEdiXFQz4fAr5PerL6kV10yJRMDafwC+mkN8mV3/dkVndFCzLCrT22fTxQ7GtrD5V9xoSD8Zi/LzLqeMnmYeTEuqTKtBTNoODqgxEhfxuh9oB4aAUvy3IPezp4ye8a+J9jlhHyOAXauBp5Mesl1/G/LG1zinm92WEX4GNgzEchiE84vfNFPLzJfdFJbiV+JlTy2++En5v8fjtvuXz72jK+Y2rP/FdJ1UHgFdm/4rndmV+g+vgv+i4p3mLQKkgAbyIQKbS/5T9M2X/c11K0U9j/DA3J6K3QRl+JD5i+WH4dBrjvxMevdXV00ssgy0ikGmM3+sielPNXmNsSysQGvjPT9ORxeOTjOs//a/q+H2xKaIb5fQnc1+Eh7C8lI7M/HXG9UvN6vi53Ht0yjgwIOKeZUX/E8zPCweUGnjFAJCkJ/pCALIumyA/ZAAdyQAqKtCmpD51K9M7mOj6Ck8wUQWqlkCT11f07G1BE+X3tK5z90pZwcjrY3528nSi/H7g+kF9fYx4Z0M4Sr1Mlp9QMJryBNyRs7vBceZ1E12fvh1dIFOZgHuSdYfD7OBxsvsLhl15f4HKCgsZcDb4eTvsJstPrCCR3WejdiZLINahPnr6TZifSFHQFH1xF3tPcq712vTvz6IudmEBpZvo2eYLM+++Ce+vezQoJaA7snjmbqGfMD+xeV7fUomRDmTnpZttHSbNTxLQQCFJSPfvmgXEc+L7kx/xFTy6w67YIueGZNxHHBCYNL9whyQ5vFKoZAnVLnzjr5W7cjjp/fPviC2ExffPr0ihHzLuuX75xM8/iA30OjGBRXZRAMk3G3H8YeL8pAMQxEcrsExNNr70ih1fufHpYiryz48RfMzP8n3Ez/H9nt38Hv/hg3fpD/R8I8LPU87/nXJJo0HE6AEEknFAUn14Jw+/ysB/Z91wCJ7WMR61+aV/fFSnv/AffuQ//JH/0qc/1Pvxe9BFPa4pqIkYaePJ7NvkJ1QDKx+1LGTfkbi1+A8pvyAwP5Ltch0xgFR58ls0U59+8L6y46n5/Ijt65WoQDEFoAOYawPXZeWpCjOOxN/J6040nnePCtgA5jkxxPPkR+oUYbbsKKKVLEzYCqx+v2bHC6ho8i0tcwzZoRtdc7xQWv+lUXL4wtJuFAOZiB6WNLOiFUqM6F2BVnoMmRMzov5Lt9zwJfnJcgA78l9aMoUYP7BpJJouCniaayPo4feyzefx093on3zpvjg/4RuW6ALZK5KVjo4WLxifXyifLHxJH9oEv+jwKiGv/ku0+ERZfp4rEKphGr0Aq6UZLqkR0ZOEhPDrmwaC75A43C6vRU1a5ilTd5aWTs6vA5O6nolNgGv6mKgH9Zb8Eg3ptdLyk0F5HZpdQoQWf+mM0TTll/LuqWHioXYrVhyL8SNvhPIbY/w0SIvKJaI6WrzHKi2d4fgRpyn6zFN5YsdtOOHX9hsYVA2VNFAUtI5BfApSx+VkjHaj+iX6yFzJT+iXcV5yqMsibszSuIpLi/GLNESfmNXrOL/+mN4vs0WRHU0H404+bSS/XlF+ogBUWbApKOkYatjLOi4MI/htFuZX1r8XoOtlYeqBFhptj/naKL9eG2MQ+YsxWj6ZfnHauUNdFAYtNrkv0xuO2abQnwSRv9AdcFlaUbYPpp57aeGuMB1DCLLqxcplBxONZto/6r4Iqx2rv2jISgVaVQgoN/M42N2uiF4OPzYz6SOQCbf1RPwg+J1Wwk/TabUiZOepYQf2uDKfw48ZiKGPhBBi7b0pE2DySdwCSLvlp7ShBlbrFJv5qgjm8ONP69p2nf5L+iPhN/QIqB8+tn6R6VVGMIdfwgZ04uOX+ddSiNCrimAePz7fGbqJ+EFG+RCNgRn40AdlBDvjOX45/DRdJuhFFGicX3fs0Qti9ATBsTwjMze2Mc2AVXjuO9HXGOHX8xpjj16SHs9NAGccggZB5p91veF2OrhUdep99OZ4SW118C80xLJoVRAchXETuEWQQU9822WsEGnyYEXM03Kg7Ps+/ErjWoF1mmV60leR6JcRIgXFrg/YgiX1lDIWycjfxK6zW+n400IG/pxxw63/ybpjIeuOW3/JuuMv6ddfRPaaZ22nxn8Lj0zPpuOffpaO9/8u44bZf/5Nxi2/mcm65e/fz3jIL9OvPy90mBr/jWWW9eDx7Ewa8vil3jCTxy8Dmfx+9suMh4itrphDxi4DemSM8es+uV78bIlfxhouSdDX+ZmAV9eJ36w4CpZTrCFScsJ6cK34HXN+myP41Ri//vXi95jz643g173m/HIOO/6/GL+3nZ91PfkV1i9vsX2I2PfaVdj3M+JDnlXPr5B9l0sS6V7V/tnC69DZv7y7UDW/Av4Z4bfJ/c/jKvmdIXJ12/FPBoap+U7wFIC7Z+9Xyc8t6F8DET9kPLoEv7OvwVPXhCb0ANmchL8p0+mB15XxC+OH3PjoQAoU3ar4zc68Ed8/5EW1NfLBowG4mM24R50fXXKi8XvWDjSyzMnOhDvgPL0pRX6z98EJ++ia2YosLJiwAx5nPESV3/2w2yC7yj5ZKaMJJhTo36+C3+wtvO9F4zKhRaAb7fSnKPO7xdMqJL10L4OfXFPRBLcq4Dd7ATyx2oXGL75wCbupBJX5XfBStbm1FkkOlJ/oObkYnx+i50qDpouhDAkGaQSV+b3ibldurUW5piLMcNBU+KFp0YG1U8FJt0EDQh1/2yvcmualTHRlfsI9y621KBct0L10A6jAD2k1D0J5N5neB+Rrjqde+Gky2E9qamX7Jw5j5rkvrOaSmKmpLanwe3IKkZ6S1mrxKlLXdm1vKH9VFiQkRZEfepEtyfxlH2I5kAxEK0O1FeaHlJpvIp0Zrrgg69THXwLFHy+1T0CdLhUhnR5/kiq/W7wUr5Nn/piBcHIVqML4YclECi1UKXAYzkUTuj1QM/CzoBX35VX5PRgIrwvknRC4JylQ2E5VMIX5Id2JJbIWbtVFYynv0iBbDVyYZmtV+R2LQrOYQPYx1WU5wg3AePyIYoHtrlAvuhVbeTZND2mglAFU5HcuSmlt5anPG9GqNukeWlF+yDbgiQd7ofqEycVdNIR9M/koNX54omuhesk7oUMUTCNvAhbm94AMFhyGnIyUfTtIo56aKOq+GIffg55YFMpVL2yJjE/AeloIX1g+qUkaxQ9Pvj7Uvb+Ow08U4iHeS96BUzIB+cn3VAtYkB8ySURmRsinRoxGFzrRRynxQzOhVbRMA93Hq3FpThHQovxuUbug971s/cJ+dkDHiGpQNX4XXGOQ4C//hOOhZAFhP0VAi/K7oFx0L9xujKYHmdrx784iF9SIvko1ftKHVMCoQmGrUo4eCeh5aX4PaCOoDcm+D4a6Cf1OEDjR72JvWv2Lsvwk8SzwlSB6iIU/HCRjpKL8/krnHZKZMOZDuqTtIxOMc7Cu/JFyB5yU5/eGb8cr9JWgA0lAdS9p4hX54ZgvjB/IDOloEPrdSCiIYojS/M7FR6wK1aAgAspOIKDuJDSMMr9ATkroLs2g4fNy0kEuNIBl+VE3kDS5OVo8uYCKj+MlsrxF+b3ietOQRwpa/FAZ7Mp77GH58RPaxSkinrGP46WELkX1S40XTrDDTV/IFAgTb8gDCHsl+c2KnSH0E3mjP1DZlIJcDZ4mQpei9kGc/kMPdmkoi8x4uPMTDlBwzzMVemn7IFqk2qVAkTAgZ9ESA1jcvvNBw3uK+o5OlIq0sVXv9vBu5H4HMUQT/bwUv3D46Bm1IlUkSZZQTJP+cSl+MyJloOmDntNDLW5FjQK0QM127TrYapjISpb0P0PtDPIygzKoj8aj+EbMBhaOHy6p2ja1NvB12LC9wIns7TSBjVMV0B8AH9ZeleI3+4YrT90upl0wNiID6EWdmML8LnrQ1CFOKDfIsc2YU6Z3eKLHHA5gzA4V5Ifzj6KM0qjQIcSaPAPRwL+JtFmU3zkI7ADNsG7qeXY0KbkBMVvAi0UqRfkd83xOodoT0QEc8sc7kZdbPD/xGGz1LDt2GIfTMzY3wxRvL/oKi/LDyiVSOyRr3SEOOgP5WhayxJJyK87vPjCydlOb/gkwxJ/0RAq7ED/0AKmLCsPHd7qKTbFbj0vwQwOYddAU2cFhSE/Th/FMXRF+szPioATbjKvwCSsgeaFYh4byo8DvfnqtANPsgpqkbGCQCMMK8XsiBoBuelH5RNCObCOwrhNTUCU//0ASQsGOZz3FD60UJ340P2QahIG1i7ouIXblRAU2EtyNUVo/Oj6Nahdk7pw2y1rzn4y4cinED+kWflqY7blW+4LHWkRCsbPBCCrxO5fOeuNVh0ZwAtqR4gWm2TuO31aAH3L/RFTCDrwr0eOnBcSJar3NdJza+iYiqOumjpxPo9XBSs6LFi8wtQGYUV/fRFNbeLLsMJXq9/HYkXGRXDAHlKDi+vQ56NuBZ5Flx3bQ0qMGQ/e30pPk+fwQPSEX7LS7+vdXqISGFQ0QQSyiqvsLzjGxejdwG3rcGmJV8yRl9EbxQ8JZ4/T4SWJlevxMWZiyNMlmB+X9ITNvkI8PU8pKQb+ekr8azQ+pFinM2lLXnRz0YHXYlm7hDTnq+5fwFpH4qTEUN3jgMn0LSj4/ZHXChIdOT/IXqouZBDUSYWvIEr8ptf/sTwDUHHz2jR1yh4ZbB2ChzP6smSfSMUx6mrzEx50oWNWN8NwqmjCPy+0fXHiIpqFnu47juHYXafTXC2X2D77/NwBa4YIGdTuVvw0ksB4nqPubYKEMP4SFuw9pc+A13R5Zgt9d0A6dA37MVvXTmxKW4wSx8/iwHL8kVPmdXUZyjeyQtOqXcyJgB3PlZh2QOoRXz+8uGDZCf4/mA0uqzhDs3KoXEjS1Lvj6LPH0q+a3gCtYhEsZJi0FoPxdoCyCdTmiaQ3AZ3GGV8vv7CGoS6kA0+hVRE+IaE/a20GCnLtRhlfJ7+wzMHQkCypO048pnBRMyQD53LqJFxNfywyvjt/Za7Dlyv6BKGY4lmoJwcyELP/oHRKGoaa5Kn4LX4OhKz/Y1KhVB0djGIYolo5oi4NINszUOwMA/nR2hfzO7pJ6B7JvB1usDoL6Jw1zsAe4JYyEb7CFjNDDhavhd4aGLhEwmsyoF1gpUgI7/g8G0foQpm502kROK+Z3hj26umtGYypICzBWozij4JMQeFosqQJ9G1G8/PN9fBx6bH64jfv/iy1SJx506D6beWC3sqknYYMztONpWxMFBNideHJx/5yc1y7Fj570vn/xBIXVnqvF63DoWsA7oPKpHAVwQxFT10xQ9ZZNXu8rRJIfbC/Ej197fuviFbr/pNvx9UTi2wzLxx1UYvVSsc2fEVXZvAs69N2AOE6Xry4ubiGeMyPP98/MnN+/dUGYgRMrcP20WF+XiuOVDGaLYX1XjKGdWsoZb473HTuwmAN1/PjJq9d30/H61ZPHl0xtWQEuDZMcN9KkISQT7F3FzJPRFI8Cnp8QUzGSiKbRQLFs4Fn1pwOMHsJwONwcEvTQL32r5gUo5m0YOkwr/E3bIjaIi2blajMF90KG/bgCj9GkJcthBmj5+LyaN/ig2UA87Ug5yVkSqyFD0HVShaoCoFfnWtKTfip2GDtApujiXf8Vc0MmRyZ38FOyw2geyhT7ditzBpXgZraCttz83k8x7+JY3wYRWDbNAY7BjOQOnaAfbXj/qnVmJpp70Z6AXtd2fJNoQxWeRBFBreEGta1YixuTGLoQSzt3QAI9C6t93yBKcoQooisMHxkSzxomG9r7qWddGpaaG8mecabtenINN6TXaLdTWDFsT3bkIlhfjUuqwElm6VQ9u0jX3k5FuYcKsb6zcZTa2Ua6jLJipnHsbjenj5vAenNlL8HSSdsoAu34ZUd7+82riw2qxNpyc3V74/CA9zyltChfPQBHu3c2Vu411ydmBMYCm5ZejKBpskK1qt81nzow3dqP2EORf77SYO6nAavbOPRT8s/TYN7GBo8YhZYR+efroUxGgqfebH70nCmW66lRUrDEFKmFJqFpnNL/VJp/njSYGt3yIZ96115xRsG0DOBW763QLDJE8vRt0iwyloQ383ZNvRA8jLqi9PrkQbNSUxTWVY21I3D41li9VBQ7YlId/g92YEHwLEEObAAAAABJRU5ErkJggg=="
    }

    override fun isSelected(): Boolean {
        return selected
    }

    override fun select() {
        selected = !selected
    }

    override fun teamName(): String {
        return "普鲁士门兴格拉德巴赫"
    }

    override fun getTeamId(): Int {
        return id
    }
}

class FavoriteTeamViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    interface IFavoriteTeamViewHolder {
        fun teamIcon(): String
        fun getTeamId(): Int
    }

    interface ItemClickListener {
        fun favoriteTeamOnRemove(data: IFavoriteTeamViewHolder, type: BettingType)
    }

    fun bind(
        iFavoriteTeam: IFavoriteTeamViewHolder,
        clickListener: ItemClickListener,
        type: BettingType
    ) {
        Glide.with(itemView.context)
            .load(iFavoriteTeam.teamIcon())
            .into(itemView.iv_add_favorite_team_icon)

        itemView.setOnClickListener {
            clickListener.favoriteTeamOnRemove(iFavoriteTeam, type)
        }
    }
}

private class MyGestureListener : GestureDetector.SimpleOnGestureListener() {

    override fun onDown(event: MotionEvent): Boolean {
        Log.d("test", "onDown: $event")
        return true
    }

    override fun onFling(
        event1: MotionEvent,
        event2: MotionEvent,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        Log.d("test", "onFling: $event1 $event2")
        return true
    }

    override fun onScroll(
        e1: MotionEvent?,
        e2: MotionEvent?,
        distanceX: Float,
        distanceY: Float
    ): Boolean {
        Log.d("test", "onScroll: $e1 $e2")
        return super.onScroll(e1, e2, distanceX, distanceY)
    }
}