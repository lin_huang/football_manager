package com.example.rxjava2demo

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import io.reactivex.rxjava3.core.Observable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import java.util.regex.Pattern


//fun main() {
//    runBlocking {
//        val numbersFlow = flowOf(1, 2, 3, 4).delayEach(1000)
//        val lettersFlow = flowOf("A", "B", "C").delayEach(2000)
//        flowOf(numbersFlow, lettersFlow).flattenMerge().collect {
//            println("smapletracing $it")
//        }
//
//        launch {
//
//            println(async {
//                1
//            }.await())
//
//            println(async {
//                delay(5000)
//                2
//            }.await())
//
//            println(async {
//                delay(1000)
//                3
//            }.await())
//
//            println(async {
//                4
//            }.await())
//        }
//    }
//}

class MainActivity : AppCompatActivity() {

    private lateinit var mainScope: Job
    private lateinit var number: String

    override fun onDestroy() {
        super.onDestroy()
//        mainScope.cancel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        web.loadUrl("https://www.google.com/?hl=zh_tw")


        val regex1 = "^(?!.*··)[\\u4e00-\\u9fa5a-zA-Z ·]{2,50}$"
        text1.setOnClickListener {
            text1.text = "12312312"
            if (type1.text.toString().isNotEmpty()) {
                if (type1.text.toString().matches(regex = regex1.toRegex())) {
                    text1.text = "true"
                } else {
                    text1.text = "false"
                }
            }
        }

        text2.setOnClickListener {
            text2.text = ""
            if (type2.text.toString().isNotEmpty()) {
                val chinese_alphabetP = Pattern.compile("^(?!.*··)[\\u4e00-\\u9fa5a-zA-Z ·]{2,50}\$")
                val chinese_alphabetM = chinese_alphabetP.matcher(type2.text.trim())

                if (chinese_alphabetM.matches()) {
                    text2.text = "true"
                } else {
                    text2.text = "false"
                }
            }
        }

        type2.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                text3.text = "char: $s / start: $start / before: $before / count: $count "
                type2.removeTextChangedListener(this)
                if (before == 0) {
                    val pattern = Pattern.compile("^(?!.*··)[\\u4e00-\\u9fa5a-zA-Z ·]$")
                    val matcher = pattern.matcher(s!![start].toString())
                    if (matcher.matches()) {
                        type2.setText(s)
                    } else {
                        type2.text = type2.text
                    }
                }
                type2.addTextChangedListener(this)
            }
        })

        for (i in 1..10) {
            number = "$i"
        }

        val viewModel = MyViewModel()
        viewModel.getUserLiveData().observe(this, Observer {
            Log.e("test", "observe trig")
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        viewModel.getUserApi(this::class.java.simpleName)

//        mainScope = MainScope().launch {
//            val list = arrayListOf<Deferred<String>>()
//
//            val time = measureTimeMillis {
//                for (i in 1..3) {
//                    println("start: $i / " + System.currentTimeMillis())
//                    val s = async {
//                        when(i) {
//                            1 -> t1()
//                            2 -> t2()
//                            3 -> t3()
//                            else -> t1()
//                        }
//                    }
//                    list.add(s)
//                }

//                for (d in list.indices) {
//                    list[d].await()
//                    println("end: $d / " + System.currentTimeMillis())
//                }

//                println("before delay: ${System.currentTimeMillis()}")
//                delay(5000)
//                println("after delay: ${System.currentTimeMillis()}")
//
//                for (i in list.awaitAll().indices) {
//                    println("end: ${list[i].await()} / " + System.currentTimeMillis())
//                }


//                println("start: " + System.currentTimeMillis())
//                val t1 = async {
//                    t1()
//                }
//                println(t1.await() + "${System.currentTimeMillis()}")
//
//                val t2 = async {
//                    t2()
//                }
//                println(t2.await() + "${System.currentTimeMillis()}")
//
//                val t3 = async {
//                    t3()
//                }
//                println(t3.await() + "${System.currentTimeMillis()}")

//            }
//            println("$time: " + System.currentTimeMillis())
//            text.text = "finish"
//        }

//        startActivity(Intent(this, SecondActivity::class.java))
//        finish()
//
//        test2(TT {
//
//        })
//
//        test3(object : ABC {
//            override fun aaa(i: Int) {
//
//            }
//        })
//
//
//        val ob1 = Observable.intervalRange(1, 3, 0, 1, TimeUnit.SECONDS)
//            .doOnNext {
//                Log.e("doOnNext", "ob1 $it")
//            }
//            .map {
//                Log.e("map", "ob1 $it")
//                "ob1 $it"
//            }
//
//
//        val ob2 = Observable.intervalRange(1, 5, 0, 2, TimeUnit.SECONDS)
//            .map {
//                Log.e("map", "ob2 $it")
//                "ob2 $it"
//            }
//
//
////        val ob1 = Observable.fromIterable(listOf(1)).timeInterval(TimeUnit.MILLISECONDS)
////        val ob2 = Observable.fromIterable(listOf(4,5,6,7)).timeInterval(TimeUnit.MILLISECONDS)
//
//        val d = zip<String, String, String>(
//            ob1.doOnComplete {
//                Log.e("test", "doOnComplete1")
//            }.doFinally {
//                Log.e("test", "doFinally1")
//            }.doOnDispose {
//                Log.e("test", "doOnDispose1")
//            },
//            ob2.doOnComplete {
//                Log.e("test", "doOnComplete2")
//            }.doFinally {
//                Log.e("test", "doFinally2")
//            }.doOnDispose {
//                Log.e("test", "doOnDispose2")
//            }, BiFunction<String, String, String> { t1: String, t2: String ->
//                Log.e("test", "BiFunction")
//                "$t1/$t2"
//            }).doOnComplete {
//            Log.e("test", "zip doOnComplete")
//        }.subscribe {
//            Log.e("test", "subscribe $it")
//        }
//
//        d.dispose()

//        Observable.create<String> {
//            it.onNext("observableEmitter_String")
//            it.onError(Throwable("error"))
//            it.onComplete()
//        }.subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(object : Observer<String> {
//                    override fun onComplete() {
//                        Log.e("test", "onComplete")
//                    }
//
//                    override fun onSubscribe(d: Disposable?) {
//                        Log.e("test", "onSubscribe")
//                    }
//
//                    override fun onNext(t: String?) {
//                        Log.e("test", "onNext $t")
//                    }
//
//                    override fun onError(e: Throwable?) {
//                        Log.e("test", "onError")
//                    }
//
//                })

//        test1().subscribe {
//            Log.e("test1 subscribe", it)
//        }
    }
}

interface ABC {
    fun aaa(i: Int);
}

suspend fun t1() = withContext(Dispatchers.IO) {
    println("t1 start: " + System.currentTimeMillis())
    delay(1000)
    println("t1 end: " + System.currentTimeMillis())
    return@withContext "t1 finish"
}

suspend fun t2() = withContext(Dispatchers.IO) {
    println("t2 start: " + System.currentTimeMillis())
    delay(2000)
    println("t2 end: " + System.currentTimeMillis())
    return@withContext "t2 finish"
}

suspend fun t3() = withContext(Dispatchers.IO) {
    println("t3 start: " + System.currentTimeMillis())
    delay(3000)
    println("t3 end: " + System.currentTimeMillis())
    return@withContext "t3 finish"
}
