package com.example.rxjava2demo;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Week {
    public static final int MONDAY = 1;
    public static final int TUESDAY = 2;
    public static final int WEDNESDAY = 3;
    public static final int SUNDAY = 7;

    @IntDef({MONDAY, TUESDAY, WEDNESDAY})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Day {

    }

    public static int getDay(@Day int day){
        switch (day) {
            case MONDAY : return MONDAY;
            case TUESDAY : return TUESDAY;
            case WEDNESDAY : return WEDNESDAY;
            default: return -1;
        }
    }
}
